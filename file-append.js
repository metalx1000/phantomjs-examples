"use strict";
var fs = require('fs');
var page = require('webpage').create(),
system = require('system'),
address;
var output = 'output.json';

//clear file
fs.write(output, '[\n', 'w');

  if (system.args.length === 1) {
    console.log('Usage: file-append.js <some URL>');
    phantom.exit(1);
  } else {
    address = system.args[1];

    page.onResourceRequested = function (req) {
      var m = JSON.stringify(req, undefined, 4);
      console.log(m);
      fs.write(output, m+",\n", 'a');
    }

    page.onResourceReceived = function (res) {
      var m = JSON.stringify(res, undefined, 4);
      console.log(m);
      fs.write(output, m+",\n", 'a');
    };

    page.open(address, function (status) {
      if (status !== 'success') {
        console.log('FAIL to load the address');
      }
      fs.write(output, '""]', 'a');
      phantom.exit();
    });
  }
