#!/usr/bin/phantomjs
var page = require('webpage').create(),
system = require('system'),
args = system.args;

if(args.length < 3){
  console.log("Usage: " + args[0] + " <email> <password>");
  console.log("Example: " + args[0] + " myemail@yahoo.com BlueMoon");
  phantom.exit(1);
}

page.open("http://www.facebook.com/login.php", function(status) {
  if (status === "success") {
    page.evaluate(function(args) {
      document.getElementById("email").value = args[1];
      document.getElementById("pass").value = args[2];
      document.getElementById("loginbutton").click();
    },args);
    window.setTimeout(function() {
      page.render("page.png");
      phantom.exit();
    }, 1000);
  }
});
